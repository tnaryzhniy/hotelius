require 'rails_helper'

RSpec.describe "Landings", type: :request do
  describe "GET /landings" do
    it "should have the content 'Sample App'" do
      visit '/landings/index'
      expect(page).to have_content('Hotelius')
    end
  end
end
