class Hotel < ActiveRecord::Base
  belongs_to :user
  has_one :address
  has_many :raitings

  accepts_nested_attributes_for :address

  mount_uploader :foto, FotoUploader

  validates :title, presence: true, length: {minimum: 5}
  validates :stars, presence: true

=begin
  def top_five
    hotels = []
    all.each do |hotel|
      hotel.raitings.average(:raiting)
    end
  end
=end

  def update_raiting
    aver = Raiting.average('raiting')
    update(aver_raiting: aver)
  end

end
