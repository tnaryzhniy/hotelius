class RaitingsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @hotel = Hotel.find(params[:hotel_id])
    @raiting = @hotel.raitings.build(raiting_params)
    @raiting.user = current_user
    @raiting.save
    @hotel.update_raiting
    redirect_to hotel_path(@hotel)
  end



  private

  def raiting_params
    params.require(:raiting).permit(:commenter, :comment, :raiting)
  end
end
